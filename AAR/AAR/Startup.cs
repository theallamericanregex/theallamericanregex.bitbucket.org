﻿using Microsoft.Owin;
using Owin;

//Mark was here

[assembly: OwinStartupAttribute(typeof(AAR.Startup))]
namespace AAR
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
