﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace AAR.Models
{
    public class Component
    {
        public int ComponentID { get; set; }
        public int ComponentNumber { get; set; }
        public string ComponentDescription { get; set; }
        public string SupplierName { get; set; }
        public int SupplierID { get; set; }
        public int SupplierPartNumber { get; set; }
        public int Quantity { get; set; }
        public virtual Part ParentPart { get; set; }
    }

    public class Part
    {
        public int PartID { get; set; }
        public string UserID { get; set; }
        public int PartNumber { get; set; }
        public string PartDescription { get; set;  }
        public virtual ICollection<Component> Components { get; set; }
    }

    public class UploadPartViewModel
    {
        [Required]
        [Display(Name = "Part Number")]
        public int PartNumber { get; set; }

        [Required]
        [Display(Name = "Part Description")]
        public string PartDescription { get; set; }

        [Required]
        [Display(Name = "Part File")]
        public HttpPostedFileBase PartFile { get; set; }
    }

    public class PartListViewModel
    {
        public IEnumerable<Part> Parts;
    }
}
