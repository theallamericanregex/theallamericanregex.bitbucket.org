﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

using AAR.Models;

namespace AAR.Controllers
{
    [Authorize]
    public class PartController : Controller
    {
        protected ApplicationDbContext ApplicationDbContext { get; set; }
        protected UserManager<ApplicationUser> UserManager { get; set; }

        public PartController()
        {
            this.ApplicationDbContext = new ApplicationDbContext();
            this.UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(this.ApplicationDbContext));
        }

        //
        // GET: /bom/
        [HttpGet]
        public ActionResult Index()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            var parts = from p in ApplicationDbContext.Parts
                        where p.UserID == user.Id
                        orderby p.PartNumber ascending
                        select p;
            var viewModel = new PartListViewModel
            {
                Parts = parts
            };
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UploadPartViewModel vm)
        {
            // These are the mime types for MS Excel Spreadsheets
            var validSpreadsheetTypes = new string[]
            {
                "application/vnd.ms-excel",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            };

            if (vm.PartFile == null || vm.PartFile.ContentLength == 0)
            {
                ModelState.AddModelError("BOMFile", "This Field is Required");
            }
            else if (!validSpreadsheetTypes.Contains(vm.PartFile.ContentType))
            {
                ModelState.AddModelError("BOMFile", "Please upload a MS Excel Spreadsheet");
            }

            if (ModelState.IsValid)
            {
                var user = UserManager.FindById(User.Identity.GetUserId());
                string filePath = Path.Combine(Server.MapPath("~/Uploads/"), vm.PartNumber + DateTime.Now.Ticks.ToString());
                vm.PartFile.SaveAs(filePath);
                string excelConnectionString = excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                if (System.IO.Path.GetExtension(vm.PartFile.FileName) == ".xls")
                {
                    excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                }
                var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", excelConnectionString);
                var ds = new DataSet();

                adapter.Fill(ds, "Sheet1");
                var data = ds.Tables["Sheet1"];
                var part = new Part
                {
                    PartNumber = vm.PartNumber,
                    PartDescription = vm.PartDescription,
                    UserID = user.Id
                };
                List<Component> components = new List<Component>(new Component[data.Rows.Count]);
                for (var i = 0; i < data.Rows.Count; i++)
                {
                    DataRow r = data.Rows[i];
                    components[i] = new Component();
                    components[i].ComponentNumber = (int) r.Field<double>("Component Number");
                    components[i].ComponentDescription = r.Field<string>("Component Description");
                    components[i].SupplierName = r.Field<string>("Supplier Name");
                    components[i].SupplierID = (int) r.Field<double>("Supplier ID");
                    components[i].SupplierPartNumber = (int) r.Field<double>("Supplier Part Number");
                    components[i].Quantity = (int) r.Field<double>("Quantity");
                    ApplicationDbContext.Components.Add(components[i]);
                }
                part.Components = components;
                ApplicationDbContext.Parts.Add(part);
                ApplicationDbContext.SaveChanges();
                return RedirectToAction("Index");
            }

            return View();
        }
	}
}