﻿using AAR.Models;
using Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAR.Controllers
{
    [Authorize]
    public class StripeController : Controller
    {
        // GET: Stripe
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Charge()
        {
            ViewBag.Message = "Learn how to process payments with Stripe";

            return View(new StripeChargeModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Charge(StripeChargeModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var chargeId = await ProcessPayment(model);
            // You should do something with the chargeId --> Persist it maybe?

            return View("../Home/Index");
        }
        private async Task<string> ProcessPayment(StripeChargeModel model)
        {
            return await Task.Run(() =>
            {
                var myCharge = new StripeChargeCreateOptions
                {
                    // convert the amount of $12.50 to pennies i.e. 1250
                    Amount = (int)(model.Amount * 100),
                    Currency = "usd",
                    Description = "Description for test charge",
                    Card = new StripeCreditCardOptions() { TokenId = model.Token }
                };

                var chargeService = new StripeChargeService("sk_test_0e0r7ddKoqYIESEenqs3yr53");
                var stripeCharge = chargeService.Create(myCharge);

                return stripeCharge.Id;
            });
        }
    }
}